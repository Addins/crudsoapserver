package com.app.domain;

import java.util.Date;
import java.util.Set;

public class AuthorDTO implements grails.plugins.dto.DTO {
    private static final long serialVersionUID = 1L;

    private Long id;
    private Integer age;
    private Set<BookDTO> books;
    private Date date;
    private String name;

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }
    public Integer getAge() { return age; }
    public void setAge(Integer age) { this.age = age; }
    public Set<BookDTO> getBooks() { return books; }
    public void setBooks(Set<BookDTO> books) { this.books = books; }
    public Date getDate() { return date; }
    public void setDate(Date date) { this.date = date; }
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AuthorDTO[");
        sb.append("\n\tid: " + this.id);
        sb.append("\n\tage: " + this.age);
        sb.append("\n\tbooks: " + this.books);
        sb.append("\n\tdate: " + this.date);
        sb.append("\n\tname: " + this.name);
        sb.append("]");
        return sb.toString();
    }
}
