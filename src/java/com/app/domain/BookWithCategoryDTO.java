package com.app.domain;

public class BookWithCategoryDTO implements grails.plugins.dto.DTO {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String category_name;
    private String title;

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }
    public String getCategory_name() { return category_name; }
    public void setCategory_name(String category_name) { this.category_name = category_name; }
    public String getTitle() { return title; }
    public void setTitle(String title) { this.title = title; }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("BookWithCategoryDTO[");
        sb.append("\n\tid: " + this.id);
        sb.append("\n\tcategory_name: " + this.category_name);
        sb.append("\n\ttitle: " + this.title);
        sb.append("]");
        return sb.toString();
    }
}
