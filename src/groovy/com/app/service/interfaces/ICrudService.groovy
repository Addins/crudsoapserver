package com.app.service.interfaces

import java.util.List

import javax.jws.WebMethod

import org.grails.cxf.utils.EndpointType
import org.grails.cxf.utils.GrailsCxfEndpoint

@GrailsCxfEndpoint(expose=EndpointType.JAX_WS_WSDL, soap12=true)
interface ICrudService<T> {
	@WebMethod
	public T save(T item) throws Exception
	@WebMethod
	public T delete(long id) throws Exception
	@WebMethod
	public List<T> getAll()
	@WebMethod
	public T get(long id)
}
