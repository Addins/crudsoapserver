package com.app.services

import java.util.List

import javax.jws.WebMethod;

import com.app.service.interfaces.ICrudService
import com.app.domain.Category
import com.app.domain.CategoryDTO
import com.app.domain.BookDTO

import org.grails.cxf.utils.EndpointType
import org.grails.cxf.utils.GrailsCxfEndpoint

@GrailsCxfEndpoint(expose=EndpointType.JAX_WS_WSDL, soap12=true)
class CategoryService implements ICrudService<CategoryDTO> {

	@WebMethod
	@Override
	public CategoryDTO save(CategoryDTO item) throws Exception {
		if(item.id<1){
			//Create new Category
			Category c = new Category();
			c.name = item.name
			c.created = Calendar.getInstance().getTime()
			c.save(flush: true)
			return c.toDTO()
		}else{
			//Update existing category
			def c = Category.get(item.id)
			c.name = item.name
			c.created = Calendar.getInstance().getTime()
			c.save(flush: true)
			return c.toDTO()
		}
	}

	@WebMethod
	@Override
	public CategoryDTO delete(long id) throws Exception {
		def item = Category.get(id)
		if(item){
		def c = new Category()
		c.name = item.name
		c.created = item.created
		item.delete(flush: true)
		return c.toDTO()
		}else{
			throw new Exception("Category Not Found")
		}
	}

	@WebMethod
	@Override
	public List<CategoryDTO> getAll() {
		List<Category> res = Category.list()
		return res.toDTO(CategoryDTO)
	}

	@WebMethod
	@Override
	public CategoryDTO get(long id) {
		def c = Category.get(id)
		if(c)
		return c.toDTO()
		else
		return null
	}
}
