package com.app.services

import java.util.Date;
import java.util.List;

import javax.jws.WebMethod

import com.app.service.interfaces.ICrudService
import com.app.domain.*

import org.grails.cxf.utils.EndpointType
import org.grails.cxf.utils.GrailsCxfEndpoint

@GrailsCxfEndpoint(expose=EndpointType.JAX_WS_WSDL, soap12=true)
class BookService implements ICrudService<BookDTO> {
	@WebMethod
	@Override
	public BookDTO save(BookDTO item) throws Exception {
		if(item.id<1){
			//Create new Book
			Book b = new Book()
			b.title = item.title
			b.description = item.description
			b.year = item.year
			b.created = Calendar.getInstance().getTime()
			if(item.category){
				Category c = new Category()
				c.name = item.category.name
				c.created = Calendar.getInstance().getTime()
//				c.save()
				b.category=c
			}
			if(item.author){
				Author a = new Author()
				a.name = item.author.name
				a.age = item.author.age
				a.date = item.author.date
//				a.save()
				b.author=a
			}
			b.save(flush: true)
			return b.toDTO()
		}else{
			//Update existing Book
			def b = Book.get(item.id)
			b.title = item.title
			b.description = item.description
			b.year = item.year
			b.created = Calendar.getInstance().getTime()
			if(item.category){
				Category c = new Category()
				c.name = item.category.name
				c.created = item.category.created
				b.category=c
			}
			if(item.author){
				Author a = new Author()
				a.name = item.author.name
				a.age = item.author.age
				a.date = item.author.date
				b.author=a
			}
			b.save(flush: true)
			return b.toDTO()
		}
	}
	@WebMethod
	@Override
	public BookDTO delete(long id) throws Exception {
		def b = Book.get(id)
		if(b){
			def de = new Book()
			de.title = b.title
			de.description = b.description
			de.year = b.year
			de.created = b.created
			if(b.category){
				Category c = Category.get(b.category.id)
				de.category=c
			}
			if(b.author){
				Author a = Author.get(b.author.id)
				de.author=a
			}
			b.delete(flush: true)
			return de.toDTO()
		}else{
			throw new Exception("Book Not Found")
		}
	}
	@WebMethod
	@Override
	public List<BookDTO> getAll() {
		return Book.list().toDTO(BookDTO)
	}
	@WebMethod
	@Override
	public BookDTO get(long id) {

		def b = Book.get(id)
		if(b)
			return b.toDTO()
		else
			return null
	}
	@WebMethod
	@Override
	public List<BookWithCategoryDTO> getAllBookWithCategory() {
		return BookWithCategory.list().toDTO(BookWithCategoryDTO)
	}
}
