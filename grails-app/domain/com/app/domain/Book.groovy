package com.app.domain

class Book {

    static constraints = {
    }
	
	static belongsTo = [author: Author]
	static hasOne = [category: Category]
	
	String title
	String description
	Integer year
	Date created
}
