package com.app.domain


class Author {

    static constraints = {
    }
	
	static hasMany = [books: Book]
	
	String name
	Integer age
	Date date
}
