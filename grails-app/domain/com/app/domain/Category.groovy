package com.app.domain


class Category {

    static constraints = {
    }
	
	static hasMany = [books:Book]
	
	String name
	Date created
}
